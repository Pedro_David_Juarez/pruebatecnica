const api = require('express').Router();
const controller = require('./menu.controller');

api.get('/', controller.getMenu);

module.exports = api;