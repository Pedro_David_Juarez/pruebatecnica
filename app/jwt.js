const jwt = require('jwt-simple');

module.exports = class JWT {
    static get key(){
        return "PSAFDUN/)=KJH%tevtryyniytbTRHu yu&%B&Y&%U7rBY/O)=?=PV&6%$yV$v%&$v4%$#%4534vDFsHjYaRNWTN";
    }

    static createToken(id){
        return jwt.encode({ id }, this.key)
    }

    static decodeToken(token){
        return jwt.decode(token, this.key);
    }
}