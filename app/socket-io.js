const socketIO = require( 'socket.io');
//const UserSession = require('./sources/session/user_session.model');
const Op = require('sequelize').Op;


var io;

exports.init = function(server){
    io = socketIO(server,{
        cors: { origin: '*' }
    });
    io.on("connection", socket => {
        exports.emiters.user_connected();
        socket.on('disconnect', () => {
           
        })
    })
};

emit = function(eventName, data = null, array = null){
    if(array && array.length){
        array.forEach(socketId => {
            io.to(socketId).emit(eventName,data);
            
        });
    }
    else
        io.emit(eventName, data);
}

exports.emiters = {
    chat_updated : (data = null, array =null) => emit("chat_updated", data, array),
    user_connected : (data = null, array =null) => emit("user_connected", data, array)
};
/* exports.specific = function(emiter, data, userIds){

    userIds.forEach(userId => {
        UserSession.findOne({
            where: {
                user_id : {
                    [Op.eq] : userId
                } 
            }
        }).then(item => {
            emiter(data, [item.socket_id])
        }).catch(error => console.log(error))
    })
} */
/* exports.listen = function(eventName){
    return Promise
} */